from django.shortcuts import render, redirect
from simulasi.models import JumlahPendudukAwal
from simulasi.models import Penduduk

def statCovid(request):

    if JumlahPendudukAwal.objects.all().count() == 0 or Penduduk.objects.filter(status_covid='Positif').count() == 0:

        persen_positif = 0
        persen_negatif = 100
        persen_pria = 0
        persen_perempuan = 0
        persen_muda = 0
        persen_dewasa = 0
        persen_tengah = 0
        persen_tua = 0

    elif Penduduk.objects.filter(status_covid='Positif').count() > 0:

        total = JumlahPendudukAwal.objects.all().jumlah + Penduduk.objects.all().count()
        
        # statistik berdasarkan status covid
        jumlah_positif = Penduduk.objects.filter(status_covid='Positif').count()
        persen_positif = jumlah_positif * 100 / total
        persen_negatif = 100 - persen_positif

        # statistik berdasarkan jenis kelamin
        jumlah_pria = Penduduk.objects.filter(status_covid='Positif', jenis_kelamin='Laki-Laki').count()
        persen_pria = jumlah_pria * 100 / jumlah_positif
        persen_perempuan = 100 - persen_pria

        # statistik berdasarkan usia
        usiaMuda = 0
        usiaDewasa = 0
        usiaTengah = 0
        usiaTua = 0

        for penduduk in Penduduk.objects.filter(status_covid='Positif'):
            if penduduk.usia <= 20:
                usiaMuda += 1

            elif penduduk.usia > 20 and penduduk.usia < 45:
                usiaDewasa += 1

            elif penduduk.usia >= 45 and penduduk.usia <= 65:
                usiaTengah += 1

            else:
                usiaTua +=1
        
        persen_muda = usiaMuda * 100 / jumlah_positif
        persen_dewasa = usiaDewasa * 100 / jumlah_positif
        persen_tengah = usiaTengah * 100 / jumlah_positif
        persen_tua = usiaTua * 100 / jumlah_positif

    response = {
        'positif': persen_positif,
        'negatif': persen_negatif,
        'pria': persen_pria,
        'perempuan': persen_perempuan,
        'muda': persen_muda,
        'dewasa': persen_dewasa,
        'tengah': persen_tengah,
        'tua': persen_tua
    }
    return render(request, 'stats.html', response)

def cobaLagi(request):
    if request.method == 'POST':
        JumlahPendudukAwal.objects.all().delete()
        Penduduk.objects.all().delete()
        return redirect('/')
    return render(request, 'stats.html')