from django.urls import path
from . import views

app_name = 'statistik'

urlpatterns = [
    path('', views.statCovid, name='persentasecovid'),
    path('', views.cobaLagi, name='reset'),
]